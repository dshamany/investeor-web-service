## **BootUp**

This is a backend service for an idea I'm working on as I'm learning the RSTY stack (Rust, SurrealDB, Tau, Yew).

### Current Implementations

*   Rocket Backend with REST APIs
*   Testing a templating implementation using Handlebars (and later Yew framework)
*   SurrealDB integration using REST APIs and Surreal QL
*   Added ability to consume APIs using “reqwest” crate
*   Added ability to consume APIs from AWS Lambda and DynamoDB for "User" APIs

### Current Challenges

*   Adding templating support for serverside rendering 

### Goal

*   Creating feature pairity with my skills in Node.js
    *   Creating Full-CRUD (JSON) APIs
    *   Integrating thirdparty API services
    *   Performing Server Side Rendering
    *   Integrating Authentication Services such as
        *   Basic Auth
        *   OAuth
        *   MFA
    *   Integrating Cookies and Sessions
    *   Utilizing Loggers
    *   Adding a test suite for the following
        *   Unit tests
        *   Integration tests
        *   Blackbox tests
    *   Running application in Docker (The easy part)
    *   Hosting the project on AWS, Azure or Google Cloud
*   Creating Full Stack pairity with MERN