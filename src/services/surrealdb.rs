#[allow(dead_code)]

//    We need to create a CRUD abstraction to map routes
// to a SurrealDB query, so we can write to the database 
// consistently e.g. receiving a JSON to /users route is
// mapped to "CREATE <entity> SET <attribute> = <value>"


use std::env;
use dotenv::dotenv;

use crate::models::{surrealdb::Surreal, metadata::{Metadata, ModifiedMetadata}};
use std::collections::HashMap;

use reqwest::Client;
use serde::Serialize;
use serde_json::Value;

pub async fn query(query: &str) -> String {
    dotenv().ok();

    let surreal_user = env::var("ROCKET_SURREAL_USER").unwrap();
    let surreal_pass = env::var("ROCKET_SURREAL_PASS").unwrap();
    let surreal_ns = env::var("ROCKET_SURREAL_NS").unwrap();
    let surreal_db = env::var("ROCKET_SURREAL_DB").unwrap();
    let surreal_url = env::var("ROCKET_SURREAL_URL").unwrap();


    let client = Client::new();
    let query = query.to_string();
    let resp = client
        .post(base_url(Some(surreal_url)).as_str())
        .basic_auth(surreal_user, Some(surreal_pass))
        .header("NS", surreal_ns)
        .header("DB", surreal_db)
        .header("accept", "application/json")
        .body(query)
        .send()
        .await.expect("response failed")
        .text()
        .await.expect("payload failed");
    resp

}

pub async fn query2(query: &str) -> Vec<Surreal<Value>> {
    dotenv().ok();

    let surreal_user = env::var("ROCKET_SURREAL_USER").unwrap();
    let surreal_pass = env::var("ROCKET_SURREAL_PASS").unwrap();
    let surreal_ns = env::var("ROCKET_SURREAL_NS").unwrap();
    let surreal_db = env::var("ROCKET_SURREAL_DB").unwrap();
    let surreal_url = env::var("ROCKET_SURREAL_URL").unwrap();

    let client = Client::new();
    let query = query.to_string();
    let resp = client
        .post(base_url(Some(surreal_url)).as_str())
        .basic_auth(surreal_user, Some(surreal_pass))
        .header("NS", surreal_ns)
        .header("DB", surreal_db)
        .header("accept", "application/json")
        .body(query)
        .send()
        .await.expect("response failed")
        .json::<Vec<Surreal<Value>>>()
        .await.expect("payload failed");
    resp

}

pub async fn query3(query: &str) -> Value {
    dotenv().ok();

    let surreal_user = env::var("ROCKET_SURREAL_USER").unwrap();
    let surreal_pass = env::var("ROCKET_SURREAL_PASS").unwrap();
    let surreal_ns = env::var("ROCKET_SURREAL_NS").unwrap();
    let surreal_db = env::var("ROCKET_SURREAL_DB").unwrap();
    let surreal_url = env::var("ROCKET_SURREAL_URL").unwrap();

    let client = Client::new();
    let query = query.to_string();
    let resp = client
        .post(base_url(Some(surreal_url)).as_str())
        .basic_auth(surreal_user, Some(surreal_pass))
        .header("NS", surreal_ns)
        .header("DB", surreal_db)
        .header("accept", "application/json")
        .body(query)
        .send()
        .await.expect("response failed")
        .json::<Value>()
        .await.expect("payload failed");
    resp

}

pub fn set_attributes(json_string: &str) -> String {
    let json_hm = serde_json::from_str::<HashMap<String, Value>>(json_string).unwrap_or_default();
    let mut attributes = String::new();
    for key in json_hm.keys() {
        if json_hm[key].to_string() != "null".to_string() {
            let s = format!("{}={},", key, json_hm[key]);
            attributes.push_str(&s);
        }
    }
    attributes.pop(); // to remove trailing comma

    format!("{}", attributes)
}

pub fn attributes_to_string<T: Serialize>(object: T) -> String {
    serde_json::to_string(&object).unwrap_or_default()
}

pub fn to_string_list(list: &Vec<String>) -> String {
    let list = list.clone();
    let mut result = String::new();

    // append items
    for item in list {
        let i = format!("\"{}\",", item);
        result.push_str(&i);
    }
    // pop last comma
    result.pop();

    result
}

pub fn set_query_attributes(map: &HashMap<String, String>) -> String {
    let mut result = String::new();
    for (k, v) in map.iter() {
        let tmp = format!("{}={},", k, v);
        result.push_str(&tmp);
    }
    result.pop();
    result
}

pub fn remove_null_values(map_str: &str) -> HashMap<String, Value> {
    let json_hm = serde_json::from_str::<HashMap<String, Value>>(map_str).unwrap_or_default();
    let mut tmp = json_hm.clone();
    for key in json_hm.keys() {
        if json_hm[key].to_string() == "null".to_string() {
            tmp.remove(key);
        }
    }
    tmp
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------
// HELPER FUNCTIONS 

pub fn base_url(url: Option<String>) -> String {
    dotenv().ok();
    let endpoint = url.unwrap_or("https://bootupdb.fly.dev/sql".to_string());
    endpoint.to_string()
}

pub fn create_query_metadata(record_id: &str) -> String {
    let metadata = Metadata::new();
    let metadata = serde_json::to_string(&metadata).unwrap_or_default();
    let query = format!("UPDATE {} MERGE {{ metadata: {} }};", record_id, metadata);
    query
}

pub fn update_query_metadata(record_id: &str) -> String {
    let metadata_update = ModifiedMetadata::new();
    let metadata_update = serde_json::to_string(&metadata_update).unwrap_or_default();
    let query = format!("UPDATE {} MERGE {{ metadata: {} }};", record_id, metadata_update);
    query
}

