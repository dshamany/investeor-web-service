#[cfg(test)]
mod tests;

#[macro_use]
extern crate rocket;
extern crate rocket_cors;
extern crate rocket_dyn_templates;

use rocket::response::content::RawHtml;
use rocket_dyn_templates::Template;

// Add routes and services
mod models;
mod routes;
mod services;

// import individual routes
use routes::backend::{exchange_code, get_code, sql};
use routes::surrealdb_functions::{
    create_user, 
    get_user_by_id, 
    create_item, 
    delete_item_by_id, 
    get_all_items,
    get_item_by_id, 
    get_item_by_user_id,
    update_item_by_id,
};

use routes::views::{
    code_exchange, 
    index, 
    login_form, 
    register_form, 
    verify, 
    get_contact,
    get_about,
    get_profile,
    get_investors,
    get_startups,
    get_talent,
    get_template,
};

// CORS
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::{ContentType, Header, Method, Status};
use rocket::{Request, Response};

#[catch(404)]
fn not_found() -> RawHtml<&'static str> {
    let content = r#"<html>
        <style>
            body {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                height: 100%;
                width: 100%;
                background-color: #222;
                color: #fff;
                font-family: "Verdana, San Serif, monospace";
            }

        </style>
        <body>
            <h1>⛔ 404 ⛔</h1>
            <h3>🚧 Page Not Found 🚧</h3>
            <p>Are you sure you are at the right place? If so, consider that this page might have been deleted.</p>
            <p>- Daniel Shamany</p>
        </body>
    </html>"#;
    RawHtml(content)
}

#[catch(500)]
fn internal_error() -> RawHtml<&'static str> {
    let content = r#"<html>
        <style>
            body {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                height: 100%;
                width: 100%;
                background-color: #222;
                color: #fff;
                font-family: "Verdana, San Serif, monospace";
            }

        </style>
        <body>
            <h1>⛔ 500 ⛔</h1>
            <h3>🚧 Internal Server Error 🚧</h3>
            <p>Something went wrong on Rocket's end.</p>
        </body>
    </html>"#;
    RawHtml(content)
}

pub struct CORS;

#[rocket::async_trait]
impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, GET, PUT, DELETE, OPTIONS",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));

        if request.method() == Method::Options {
            let body = "";
            response.set_header(ContentType::Plain);
            response.set_sized_body(body.len(), std::io::Cursor::new(body));
            response.set_status(Status::Ok);
        }
    }
}

#[launch]
fn rocket() -> _ {
    // RUN SERVER
    rocket::build()
        .register("/", catchers![not_found, internal_error])
        .attach(Template::fairing())
        .mount("/",
        routes![
            get_about,
            get_contact,
            get_profile,
            get_investors,
            get_startups,
            get_talent,
            get_template,
        ])
        .mount(
            "/api/auth",
            routes![get_code, exchange_code],
        )
        .attach(CORS)
        .mount(
            "/api/v1",
            routes![
                sql,
                create_user,
                get_user_by_id,
                create_item,
                update_item_by_id,
                get_all_items,
                get_item_by_id,
                get_item_by_user_id,
                delete_item_by_id,
            ],
        )
        .mount("/", routes![index, verify, login_form, register_form, code_exchange])
}
