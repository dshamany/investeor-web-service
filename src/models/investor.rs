use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::models::metadata::Metadata;


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewInvestor {
    id: Option<String>,
    user_id: String,
    bio: Option<String>,
    investments: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

impl NewInvestor {
    pub fn update_metadata(&mut self) -> Self {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            },
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}


// An investor class that demonstrates the minimum
// required information for display
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Investor {
    id: Option<String>,
    user_id: Option<String>,
    bio: Option<String>,
    investments: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

// Methods to create, update and append attributes
impl Investor {
    pub fn new(user_id: &str) -> Investor {        
        Investor {
            id: Some(Uuid::new_v4().to_string()),
            user_id: Some(user_id.to_string()),
            bio: None,
            investments: Some(Vec::new()),
            metadata: Some(Metadata::new())
        }
    }

    pub fn get_id(&self) -> String {
        match &self.id {
            Some(s) => s.clone(),
            None => "".to_string()
        }
    }

    pub fn get_user_id(&self) -> String {
        self.user_id.clone().unwrap_or_default()
    }

    pub fn get_bio(&self) -> String {
        self.bio.clone().unwrap_or_default()
    }

    pub fn get_investments_str(&self) -> String {
        let mut investments = String::new();

        for i in &self.get_investments() {
            investments.push_str(&i);
            investments.push(',');
        }
        investments.pop();

        investments
    }
    
    pub fn get_investments(&self) -> Vec<String> {
        self.investments.clone().unwrap_or_default()
    }

    pub fn set_bio(&mut self, new_bio: &str) -> Investor {
        self.bio = Some(new_bio.to_string());
        self.update_metadata()
    }

    pub fn push_investment(&mut self, company_id: &str) -> Investor {
        let mut investments = self.get_investments();
        investments.push(company_id.to_string());
        self.investments = Some(investments);
        self.update_metadata()
    }
    
    pub fn pop_investment(&mut self, company_id: &str) -> Investor {
        let mut investments = self.get_investments();
        // simplify the process by equating the types
        let company_id = company_id.to_string();

        let index = investments
            .iter()
            .position(|i: &String| i == &company_id).unwrap();
        investments.remove(index);
        self.investments = Some(investments);
        self.update_metadata()
    }

    pub fn update_metadata(&mut self) -> Investor {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            },
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }

}