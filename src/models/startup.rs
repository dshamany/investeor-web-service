#[allow(dead_code)]

use rocket::serde::{Deserialize, Serialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Startup {
    id: Option<String>,
    name: Option<String>,
    founders: Option<Vec<String>>,
    projects: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

impl Startup {
    pub fn new() -> Self {
        Startup {
            id: None,
            name: None,
            founders: None,
            projects: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_name(&self) -> String {
        self.name.clone().unwrap_or_default()
    }

    pub fn get_founders(&self) -> Vec<String> {
        self.founders.clone().unwrap_or_default()
    }

    pub fn get_projects(&self) -> Vec<String> {
        self.projects.clone().unwrap_or_default()
    }

    pub fn set_name(&mut self, value: &str) {
        self.name = Some(value.to_string());
    }

    pub fn set_founders(&mut self, value: &Vec<String>) {
        self.founders = Some(value.clone());
    }

    pub fn push_founder(&mut self, founder_id: &str) {
        let mut tmp = self.founders.clone().unwrap_or_default();
        tmp.push(founder_id.to_string());
        self.founders = Some(tmp);
    }

    pub fn pop_founder(&mut self, founder_id: &str) {
        let mut tmp = self.founders.clone().unwrap_or_default();
        let pos = tmp.binary_search(&founder_id.to_string());

        match pos {
            Ok(p) => {
                tmp.remove(p);
                self.founders = Some(tmp);
            },
            Err(e) => {
                println!("{:?}", e);
            }
        }
    }

    pub fn set_projects(&mut self, value: &Vec<String>) {
        self.projects = Some(value.clone());
    }

    pub fn push_project(&mut self, project_id: &str) {
        let mut tmp = self.projects.clone().unwrap_or_default();
        tmp.push(project_id.to_string());
        self.projects = Some(tmp);
    }

    pub fn pop_project(&mut self, project_id: &str) {
        let mut tmp = self.projects.clone().unwrap_or_default();
        let pos = tmp.binary_search(&project_id.to_string());

        match pos {
            Ok(p) => {
                tmp.remove(p);
                self.projects = Some(tmp);
            },
            Err(e) => {
                println!("{:?}", e);
            }
        }
    }

    pub fn set_metadata(&mut self, value: &Metadata) {
        self.metadata = Some(value.clone());
    }

    pub fn update_metadata(&mut self) {
        self.metadata = Some(self.metadata.clone().unwrap().update_modified())
    }

    pub fn to_string_list(list: &Vec<String>) -> String {
        let list = list.clone();
        let mut result = String::new();

        // set beginning bracket
        result.push('[');

        // append items
        for item in list {
            let i = format!("\"{}\",", item);
            result.push_str(&i);
        }

        // pop last comma
        result.pop();

        // append closing bracket
        result.push(']');

        result
    }
}
