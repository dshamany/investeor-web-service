#[allow(dead_code)]

use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Surreal<T> {
    pub time: Option<String>,
    pub status: Option<String>,
    pub result: Option<Vec<T>>,
    pub detail: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Query {
    query: String
}

impl Query {
    pub fn to_string(&self) -> String {
        self.query.clone()
    }
}