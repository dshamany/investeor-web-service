use rocket::serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::metadata::Metadata;

use crate::services::surrealdb::{query2, set_attributes};

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct BasicInfo {
    resume_id: Option<String>,
    id: Option<String>,
    firstname: Option<String>,
    lastname: Option<String>,
    metadata: Option<Metadata>,
}

impl BasicInfo {
    pub fn new(resume_id: &str) -> BasicInfo {
        BasicInfo {
            id: Some(Uuid::new_v4().to_string()),
            resume_id: Some(resume_id.to_string()),
            firstname: None,
            lastname: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_resume_id(&self) -> String {
        self.resume_id.clone().unwrap_or_default()
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or("".to_string())
    }

    pub fn get_firstname(&self) -> Option<String> {
        self.firstname.clone()
    }

    pub fn get_lastname(&self) -> Option<String> {
        self.lastname.clone()
    }

    pub fn generate_id(&mut self) {
        self.id = Some(Uuid::new_v4().to_string());
        self.update_metadata();
    }

    pub fn set_resume_id(&mut self, resume_id: &str) {
        self.resume_id = Some(resume_id.to_string());
        self.update_metadata();
    }

    pub fn set_firstname(&mut self, firstname: &str) {
        self.firstname = Some(String::from(firstname));
        self.update_metadata();
    }

    pub fn set_lastname(&mut self, lastname: &str) {
        self.lastname = Some(String::from(lastname));
        self.update_metadata();
    }

    fn update_metadata(&mut self) -> BasicInfo {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            }
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Contact {
    id: Option<String>,
    resume_id: Option<String>,
    phone: Option<String>,
    email: Option<String>,
    metadata: Option<Metadata>,
}

impl Contact {
    pub fn new(resume_id: &str) -> Contact {
        Contact {
            id: Some(Uuid::new_v4().to_string()),
            resume_id: Some(resume_id.to_string()),
            phone: None,
            email: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or("".to_string())
    }

    pub fn get_resume_id(&self) -> String {
        self.resume_id.clone().unwrap_or("".to_string())
    }

    pub fn get_phone(&self) -> Option<String> {
        self.phone.clone()
    }

    pub fn get_email(&self) -> Option<String> {
        self.email.clone()
    }

    pub fn generate_id(&mut self) {
        self.id = Some(Uuid::new_v4().to_string());
        self.update_metadata();
    }

    pub fn set_resume_id(&mut self, resume_id: &str) {
        self.resume_id = Some(resume_id.to_string());
        self.update_metadata();
    }

    pub fn set_phone(&mut self, phone_number: &str) {
        self.phone = Some(String::from(phone_number));
        self.update_metadata();
    }

    pub fn set_email(&mut self, email_address: &str) {
        self.email = Some(String::from(email_address));
        self.update_metadata();
    }

    fn update_metadata(&mut self) -> Contact {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            }
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Experience {
    id: Option<String>,
    resume_id: Option<String>,
    company: Option<String>,
    begin_date: Option<String>,
    end_date: Option<String>,
    location: Option<String>,
    description: Option<String>,
    skills: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

impl Experience {
    pub fn new(resume_id: &str) -> Experience {
        Experience {
            id: Some(Uuid::new_v4().to_string()),
            resume_id: Some(resume_id.to_string()),
            company: None,
            begin_date: None,
            end_date: None,
            location: None,
            description: None,
            skills: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_resume_id(&self) -> String {
        self.resume_id.clone().unwrap_or_default()
    }

    pub fn company(&self) -> String {
        self.company.clone().unwrap_or_default()
    }

    pub fn get_begin_date(&self) -> String {
        self.begin_date.clone().unwrap_or_default()
    }

    pub fn get_end_date(&self) -> String {
        self.end_date.clone().unwrap_or_default()
    }

    pub fn location(&self) -> String {
        self.location.clone().unwrap_or_default()
    }

    pub fn description(&self) -> String {
        self.description.clone().unwrap_or_default()
    }

    pub fn skills(&self) -> Vec<String> {
        self.skills.clone().unwrap_or_default()
    }

    pub fn generate_id(&mut self) {
        self.id = Some(Uuid::new_v4().to_string());
        self.update_metadata();
    }

    pub fn set_resume_id(&mut self, resume_id: &str) {
        self.resume_id = Some(resume_id.to_string());
        self.update_metadata();
    }

    pub fn set_company(&mut self, company: &str) {
        self.company = Some(String::from(company));
        self.update_metadata();
    }

    pub fn set_begin_date(&mut self, begin_date: &str) {
        self.begin_date = Some(String::from(begin_date));
        self.update_metadata();
    }

    pub fn set_end_date(&mut self, end_date: &str) {
        self.end_date = Some(String::from(end_date));
        self.update_metadata();
    }

    pub fn set_location(&mut self, location: &str) {
        self.location = Some(String::from(location));
        self.update_metadata();
    }

    pub fn set_description(&mut self, description: &str) {
        self.description = Some(String::from(description));
        self.update_metadata();
    }

    pub fn push_skill(&mut self, skill_id: &str) {
        let mut tmp = self.skills.clone().unwrap_or_default();
        tmp.push(skill_id.to_string());
        self.skills = Some(tmp);
        self.update_metadata();
    }

    pub fn pop_skill(&mut self, skill_id: &str) {
        let tmp = self.skills.clone();
        match tmp {
            Some(mut list) => {
                let pos = list.binary_search(&skill_id.to_string());
                if pos.is_ok() {
                    list.remove(pos.unwrap());
                    self.skills = Some(list);
                }
                self.update_metadata();
            }
            None => {
                return;
            }
        }
    }

    pub fn set_skills(&mut self, skills: &Vec<String>) {
        self.skills = Some(skills.clone());
        self.update_metadata();
    }

    fn update_metadata(&mut self) -> Experience {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            }
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub enum Degree {
    #[default]
    PreAssociate,
    Associate,
    Bachelor,
    Master,
    PhD,
}

// This is where we continue implementing getters and setters
#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Education {
    id: Option<String>,
    resume_id: Option<String>,
    institution: Option<String>,
    degree: Option<Degree>,
    begin_date: Option<String>,
    end_date: Option<String>,
    metadata: Option<Metadata>,
}

impl Education {
    pub fn new(resume_id: &str) -> Education {
        Education {
            id: Some(Uuid::new_v4().to_string()),
            resume_id: Some(resume_id.to_string()),
            institution: None,
            degree: None,
            begin_date: None,
            end_date: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_institution(&self) -> String {
        self.institution.clone().unwrap_or_default()
    }

    pub fn get_degree(&self) -> Option<Degree> {
        self.degree.clone()
    }

    pub fn get_begin_date(&self) -> String {
        self.begin_date.clone().unwrap_or_default()
    }

    pub fn get_end_date(&self) -> String {
        self.end_date.clone().unwrap_or_default()
    }

    pub fn generate_id(&mut self) {
        self.id = Some(Uuid::new_v4().to_string());
        self.update_metadata();
    }

    pub fn set_resume_id(&mut self, resume_id: &str) {
        self.resume_id = Some(resume_id.to_string());
        self.update_metadata();
    }

    pub fn set_institution(&mut self, institution: &str) {
        self.institution = Some(String::from(institution));
        self.update_metadata();
    }

    pub fn set_degree(&mut self, degree: &Degree) {
        self.degree = Some(degree.clone());
        self.update_metadata();
    }

    pub fn set_begin_date(&mut self, begin_date: &str) {
        self.begin_date = Some(String::from(begin_date));
        self.update_metadata();
    }

    pub fn set_end_date(&mut self, end_date: &str) {
        self.end_date = Some(String::from(end_date));
        self.update_metadata();
    }

    fn update_metadata(&mut self) -> Education {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            }
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Project {
    id: Option<String>,
    resume_id: Option<String>,
    name: Option<String>,
    description: Option<String>,
    links: Option<Vec<String>>,
    tags: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

impl Project {
    pub fn new(resume_id: &str) -> Project {
        Project {
            id: Some(Uuid::new_v4().to_string()),
            resume_id: Some(resume_id.to_string()),
            name: None,
            description: None,
            links: None,
            tags: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or("".to_string())
    }

    pub fn get_name(&self) -> String {
        self.name.clone().unwrap_or_default()
    }

    pub fn get_description(&self) -> String {
        self.description.clone().unwrap_or_default()
    }

    pub fn get_links(&self) -> Vec<String> {
        self.links.clone().unwrap_or_default()
    }

    pub fn get_tags(&self) -> Vec<String> {
        self.tags.clone().unwrap_or_default()
    }

    pub fn generate_id(&mut self) {
        self.id = Some(Uuid::new_v4().to_string());
        self.update_metadata();
    }

    pub fn set_resume_id(&mut self, resume_id: &str) {
        self.resume_id = Some(resume_id.to_string());
        self.update_metadata();
    }

    pub fn set_name(&mut self, name: &str) {
        self.name = Some(String::from(name));
        self.update_metadata();
    }

    pub fn set_description(&mut self, description: &str) {
        self.description = Some(String::from(description));
        self.update_metadata();
    }

    pub fn set_links(&mut self, links: &Vec<String>) {
        self.links = Some(links.clone());
        self.update_metadata();
    }

    pub fn push_tag(&mut self, tag_id: &str) {
        let mut tmp = self.tags.clone().unwrap_or(Vec::new());
        tmp.push(tag_id.to_string());
        self.tags = Some(tmp);
        self.update_metadata();
    }

    pub fn pop_tag(&mut self, tag_id: &str) {
        let tmp = self.tags.clone();
        match tmp {
            Some(mut list) => {
                let pos = list.binary_search(&tag_id.to_string());
                if pos.is_ok() {
                    list.remove(pos.unwrap());
                    self.tags = Some(list);
                }
                self.update_metadata();
            }
            None => {
                return;
            }
        }
    }

    pub fn update_metadata(&mut self) -> Project {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            }
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct NewSimpleResume {
    id: Option<String>,
    pub user_id: String,
    pub basic_info: Option<BasicInfo>,
    pub contact: Option<Contact>,
    pub experience: Option<Vec<Experience>>,
    pub education: Option<Vec<Education>>,
    pub project: Option<Vec<Project>>,
    metadata: Option<Metadata>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct SimpleResume {
    id: Option<String>,
    user_id: Option<String>,
    basic_info: Option<String>,
    contact: Option<String>,
    experience: Option<Vec<String>>,
    education: Option<Vec<String>>,
    project: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

impl SimpleResume {
    pub fn new(user_id: &str) -> SimpleResume {
        SimpleResume {
            id: Some(Uuid::new_v4().to_string().replace("-", "")[0..20].to_string()),
            user_id: Some(user_id.to_string()),
            basic_info: None,
            contact: None,
            experience: None,
            education: None,
            project: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_user_id(&self) -> String {
        self.user_id.clone().unwrap_or_default()
    }

    pub fn get_basic_info(&self) -> String {
        self.basic_info.clone().unwrap_or_default()
    }

    pub fn get_contact(&self) -> String {
        self.contact.clone().unwrap_or_default()
    }

    pub fn get_experience(&self) -> Vec<String> {
        self.experience.clone().unwrap_or_default()
    }

    pub fn get_education(&self) -> Vec<String> {
        self.education.clone().unwrap_or_default()
    }

    pub fn get_projects(&self) -> Vec<String> {
        self.project.clone().unwrap_or_default()
    }

    pub fn get_experience_str(&self) -> String {
        let mut experience = String::new();

        for i in &self.get_experience() {
            experience.push_str(&i);
            experience.push(',');
        }
        experience.pop();

        experience
    }

    pub fn get_education_str(&self) -> String {
        let mut education = String::new();

        for i in &self.get_education() {
            education.push_str(&i);
            education.push(',');
        }
        education.pop();

        education
    }

    pub fn get_project_str(&self) -> String {
        let mut project = String::new();

        for i in &self.get_projects() {
            project.push_str(&i);
            project.push(',');
        }
        project.pop();

        project
    }

    pub async fn set_basic_info(&mut self, value: Option<BasicInfo>) -> SimpleResume {
        match value {
            Some(mut v) => {
                v.update_metadata();
                let json_str = serde_json::to_string(&v).unwrap_or_default();
                let attributes = set_attributes(&json_str);
                let q = format!("CREATE basic_info:{} SET {};", self.get_id(), attributes);
                let resp = query2(&q).await;
                let resp = resp[0].result.clone().unwrap_or_default();
                self.basic_info = Some(resp[0]["id"].to_string().replace("\"", ""));
                return self.to_owned();
            }
            None => self.to_owned()
        }
    }

    pub async fn set_contact(&mut self, value: Option<Contact>) -> SimpleResume {
        match value {
            Some(mut v) => {
                v.update_metadata();
                let json_str = serde_json::to_string(&v).unwrap_or_default();
                let attributes = set_attributes(&json_str);
                let q = format!("CREATE contact:{} SET {}", self.get_id(), attributes);
                let resp = query2(&q).await;
                let resp = resp[0].result.clone().unwrap_or_default();
                self.contact = Some(resp[0]["id"].to_string().replace("\"", ""));
                self.update_metadata();
                self.to_owned()
            }
            None => self.to_owned(),
        }
    }

    pub fn push_experience(&mut self, experience_id: &str) {
        let mut tmp = self.experience.clone().unwrap_or_default();
        tmp.push(experience_id.to_string());
        self.experience = Some(tmp);
        self.update_metadata();
    }

    pub fn push_education(&mut self, education_id: &str) {
        let mut tmp = self.education.clone().unwrap_or_default();
        tmp.push(education_id.to_string());
        self.education = Some(tmp);
        self.update_metadata();
    }

    pub fn push_project(&mut self, project_id: &str) {
        let mut tmp = self.project.clone().unwrap_or_default();
        tmp.push(project_id.to_string());
        self.project = Some(tmp);
        self.update_metadata();
    }

    pub async fn set_experience(&mut self, list: Option<Vec<Experience>>) -> SimpleResume {
        match list {
            Some(object_list) => {
                for item in object_list {
                    let item = item.clone().update_metadata();
                    let item = serde_json::to_string(&item).unwrap();
                    let attributes = set_attributes(&item);
                    let q = format!("CREATE experience:{} SET {}", self.get_id(), attributes);
                    let resp = query2(&q).await;
                    let resp = resp[0].result.clone().unwrap_or_default();

                    if !resp.is_empty() {
                        self.push_experience(&resp[0]["id"].to_string().replace("\"", ""));
                    }
                }
                return self.to_owned();
            }
            None => {
                return self.to_owned();
            }
        }
    }

    pub async fn set_education(&mut self, list: Option<Vec<Education>>) -> SimpleResume {
        match list {
            Some(object_list) => {
                for item in object_list {
                    let item = item.clone().update_metadata();
                    let item = serde_json::to_string(&item).unwrap();
                    let attributes = set_attributes(&item);
                    let q = format!("CREATE education:{} SET {}", self.get_id(), attributes);
                    let resp = query2(&q).await;
                    let resp = resp[0].result.clone().unwrap_or_default();

                    if !resp.is_empty() {
                        self.push_education(&resp[0]["id"].to_string().replace("\"", ""));
                    }
                }
                return self.to_owned();
            }
            None => {
                return self.to_owned();
            }
        }
    }

    pub async fn set_project(&mut self, list: Option<Vec<Project>>) -> SimpleResume {
        match list {
            Some(object_list) => {
                for item in object_list {
                    let item = item.clone().update_metadata();
                    let item = serde_json::to_string(&item).unwrap();
                    let attributes = set_attributes(&item);
                    let q = format!("CREATE project SET {}", attributes);
                    let resp = query2(&q).await;
                    let resp = resp[0].result.clone().unwrap_or_default();

                    if !resp.is_empty() {
                        self.push_project(&resp[0]["id"].to_string().replace("\"", ""));
                    }
                }
                self.update_metadata();
                return self.to_owned();
            }
            None => {
                return self.to_owned();
            }
        }
    }

    pub fn update_metadata(&mut self) -> SimpleResume {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            }
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}
