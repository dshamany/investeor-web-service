#[allow(dead_code)]


use rocket::serde::{Deserialize, Serialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Comment {
    id: Option<String>,
    owner_id: Option<String>,
    post_id: Option<String>,
    content: Option<String>,
    metadata: Option<Metadata>,
}

impl Comment {
    pub fn new() -> Self {
        Comment {
            id: None,
            owner_id: None,
            post_id: None,
            content: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_owner_id(&self) -> String {
        self.owner_id.clone().unwrap_or_default()
    }

    pub fn get_post_id(&self) -> String {
        self.post_id.clone().unwrap_or_default()
    }

    pub fn get_content(&self) -> String {
        self.content.clone().unwrap_or_default()
    }

    pub fn get_metadata(&self) -> Metadata {
        self.metadata.clone().unwrap_or_default()
    }

    pub fn set_content(&mut self, value: &str) {
        self.content = Some(value.to_string())
    }

    pub fn set_metadata(&mut self, value: Metadata) {
        self.metadata = Some(value)
    }

    pub fn update_metadata(&mut self) {
        self.metadata = Some(self.metadata.clone().unwrap().update_modified())
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewComment {
    id: Option<String>,
    owner_id: String,
    post_id: String,
    content: String,
    metadata: Option<Metadata>,
}
