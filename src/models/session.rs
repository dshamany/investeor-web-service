#[allow(dead_code)]

use rand::prelude::*;
use chrono::{Duration, Utc};
use serde::{Deserialize, Serialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Session {
    id: Option<String>,
    user_id: Option<String>,
    code: Option<String>,
    expiration: Option<i64>,
    metadata: Option<Metadata>,
}

impl Session {
    pub fn new(user_id: String) -> Self {
        Session {
            id: None,
            user_id: Some(user_id),
            code: Some(Self::generate_code()),
            expiration: Some((chrono::Utc::now() + Duration::days(7)).timestamp()),
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_code(&self) -> String {
        self.code.clone().unwrap_or_default()
    }

    pub fn is_expired(&self) -> bool {
        let now = Utc::now().timestamp();

        // future - past
        let diff = self.expiration.unwrap_or(0) - now;

        diff < 0
    }

    pub fn generate_code() -> String {
        let mut rng = rand::thread_rng();
        let code: i32 = rng.gen_range(0..=999999);
        format!("{:06}", code)
    }
}
