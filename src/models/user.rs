use rocket::serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, PartialOrd, Eq, Ord)]pub enum UserProfile {
    User,
    Investor,
    Entrepreneur,
    Talent
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct User {
    pub id: Option<String>,
    photo: Option<String>,
    fullname: Option<String>,
    bio: Option<String>,
    email: Option<String>,
    password: Option<String>,
    profiles: Option<Vec<UserProfile>>,
    metadata: Option<Metadata>,
}

impl User {
    pub fn new() -> User {
        User {
            id: Some(Uuid::new_v4().to_string()),
            photo: None,
            fullname: None,
            bio: None,
            email: None,
            password: None,
            profiles: Some(vec![UserProfile::User]),
            metadata: Some(Metadata::new())
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_photo(&self) -> String {
        self.photo.clone().unwrap_or_default()
    }

    pub fn get_fullname(&self) -> String {
        self.fullname.clone().unwrap_or_default()
    }

    pub fn get_bio(&self) -> String {
        self.bio.clone().unwrap_or_default()
    }

    pub fn get_email(&self) -> String {
        self.email.clone().unwrap_or_default()
    }

    pub fn get_password(&self) -> String {
        self.password.clone().unwrap_or_default()
    }

    pub fn get_profiles(&self) -> Vec<UserProfile> {
        self.profiles.clone().unwrap_or_default()
    }

    pub fn set_photo(&mut self, value: &str) -> User {
        let new_photo_value = value.to_string();
        self.fullname = Some(new_photo_value);
        self.update_metadata()
    }

    pub fn set_fullname(&mut self, value: &str) -> User {
        let new_fullname_value = value.to_string();
        self.fullname = Some(new_fullname_value);
        self.update_metadata()
    }

    pub fn set_bio(&mut self, value: &str) -> User {
        let new_bio_value = value.to_string();
        self.bio = Some(new_bio_value);
        self.update_metadata()
    }

    pub fn set_email(&mut self, value: &str) -> User {
        let new_email_value = value.to_string();
        self.email = Some(new_email_value);
        self.update_metadata()
    }

    pub fn set_password(&mut self, value: &str) -> User {
        let new_password = value.to_string();
        self.email = Some(new_password);
        self.update_metadata()
    }

    pub fn push_profile(&mut self, user_profile: &UserProfile) {
        let mut tmp = self.profiles.clone().unwrap_or_default();
        tmp.push(user_profile.clone());
        self.profiles = Some(tmp);
    }

    pub fn pop_profile(&mut self, user_profile: &UserProfile) {
        let mut tmp = self.profiles.clone().unwrap_or_default();
        let pos = tmp.binary_search(user_profile);
        match pos {
            Ok(p) => {
                tmp.remove(p);
                self.profiles = Some(tmp);
            },
            _ => return
        }
    }

    pub fn set_profiles(&mut self, value: Option<Vec<UserProfile>>) -> User {
        let mut tmp = self.profiles.clone().unwrap_or_default();

        match value {
            Some(v) => {
                for profile in v {
                    tmp.push(profile);
                }
                self.profiles = Some(tmp);
                self.update_metadata()
            },
            None => self.to_owned()
        }

    }

    pub fn update_metadata(&mut self) -> User {
        match &self.metadata {
            Some(_md) => {
                let mut metadata = self.metadata.clone().unwrap();
                metadata.update_modified();
                self.metadata = Some(metadata);
                return self.to_owned();
            },
            None => {
                self.metadata = Some(Metadata::new());
                return self.to_owned();
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewUser {
    pub fullname: String,
    pub email: String,
    pub password: String,
    pub profiles: Vec<UserProfile>,
    pub metadata: Option<Metadata>,
    pub code: String
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserToken {
    pub code: Option<String>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserCredentials {
    pub email: Option<String>,
    pub password: Option<String>
}

