#[allow(dead_code)]

use rocket::serde::{Deserialize, Serialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum LectureLevel {
    Beginner,
    Intermediate,
    Advanced
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Lecture {
    pub id: Option<String>,
    pub title: String,
    pub video_urls: Option<String>,
    pub image_urls: Option<String>,
    pub text_content: Option<String>,
    pub links: Option<String>,
    pub level: Option<LectureLevel>,
    pub metadata: Option<Metadata>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewLecture {
    pub id: Option<String>,
    pub title: String,
    pub video_urls: Option<String>,
    pub image_urls: Option<String>,
    pub text_content: Option<String>,
    pub links: Option<String>,
    pub level: Option<LectureLevel>,
    pub metadata: Option<Metadata>
}


