#[allow(dead_code)]

use rocket::serde::{Deserialize, Serialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Post {
    id: Option<String>,
    title: Option<String>,
    image_url: Option<String>,
    content: Option<String>,
    metadata: Option<Metadata>,
}

impl Post {
    pub fn new() -> Self {
        Post {
            id: None,
            title: None,
            image_url: None,
            content: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_title(&self) -> String {
        self.title.clone().unwrap_or_default()
    }

    pub fn get_image_url(&self) -> String {
        self.image_url.clone().unwrap_or_default()
    }

    pub fn get_content(&self) -> String {
        self.content.clone().unwrap_or_default()
    }

    pub fn get_metadata(&self) -> Metadata {
        self.metadata.clone().unwrap_or_default()
    }

    pub fn set_title(&mut self, value: &str) {
        self.title = Some(value.to_string())
    }

    pub fn set_image_url(&mut self, value: &str) {
        self.image_url = Some(value.to_string())
    }

    pub fn set_content(&mut self, value: &str) {
        self.content = Some(value.to_string())
    }

    pub fn set_metadata(&mut self, value: Metadata) {
        self.metadata = Some(value)
    }

    pub fn update_metadata(&mut self) {
        self.metadata = Some(self.metadata.clone().unwrap().update_modified())
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewPost {
    pub id: Option<String>,
    pub title: Option<String>,
    pub image_url: Option<String>,
    pub content: Option<String>,
    pub metadata: Option<Metadata>,
}