#[allow(dead_code)]

use std::collections::HashMap;

use rocket::serde::{Deserialize, Serialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum MemberPermission {
    READ,
    EDIT,
    FULL,
    ADMIN
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Project {
    id: Option<String>,
    startup_id: Option<String>,
    name: Option<String>,
    description: Option<String>,
    members: Option<HashMap<String, MemberPermission>>,
    post_ids: Option<Vec<String>>,
    metadata: Option<Metadata>,
}

impl Project {
    pub fn new() -> Self {
        Project {
            id: None,
            startup_id: None,
            name: None,
            description: None,
            members: None,
            post_ids: None,
            metadata: Some(Metadata::new()),
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_startup_id(&self) -> String {
        self.startup_id.clone().unwrap_or_default()
    }

    pub fn get_description(&self) -> String {
        self.description.clone().unwrap_or_default()
    }

    pub fn get_members(&self) -> HashMap<String, MemberPermission> {
        self.members.clone().unwrap_or_default()
    }

    pub fn get_post_ids(&self) -> Vec<String> {
        self.post_ids.clone().unwrap_or_default()
    }

    pub fn get_metadata(&self) -> Metadata {
        self.metadata.clone().unwrap_or_default()
    }

    pub fn set_description(&mut self, value: &str) {
        self.description = Some(value.to_string());
    }

    pub fn push_member(&mut self, id: &str, permissions: &MemberPermission) {
        let mut tmp = self.members.clone().unwrap_or_default();
        tmp.insert(id.to_string(), permissions.clone());
        self.members = Some(tmp);
    }

    pub fn pop_member(&mut self, id: &str) {
        let mut tmp = self.members.clone().unwrap_or_default();
        tmp.remove(id);
        self.members = Some(tmp);
    }

    pub fn push_post_id(&mut self, id: &str) {
        let mut tmp = self.post_ids.clone().unwrap_or_default();
        tmp.push(id.to_string());
        self.post_ids = Some(tmp);
    }

    pub fn pop_post_id(&mut self, id: &str) {
        let mut tmp = self.post_ids.clone().unwrap_or_default();
        let pos = tmp.binary_search(&id.to_string()).unwrap_or_default();

        if pos != usize::default() {
            tmp.remove(pos);
        }
        self.post_ids = Some(tmp);
    }

    pub fn set_metadata(&mut self, value: Metadata) {
        self.metadata = Some(value);
    }

    pub fn update_metadata(&mut self) {
        self.metadata = Some(self.metadata.clone().unwrap().update_modified())
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewProject {
    pub id: Option<String>,
    pub startup_id: String,
    pub name: String,
    pub description: Option<String>,
    pub members: Option<HashMap<String, Vec<MemberPermission>>>,
    pub post_ids: Option<Vec<String>>,
    pub metadata: Option<Metadata>,
}
