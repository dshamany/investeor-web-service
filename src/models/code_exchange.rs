#[allow(dead_code)]

use serde::{Serialize, Deserialize};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CodeExchange {
    pub code: Option<String>
}

impl CodeExchange {
    fn new() -> Self {
        CodeExchange { code: None }
    }
}