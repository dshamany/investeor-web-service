use rocket::serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CardView {
    pub id: Option<String>,
    pub image: Option<String>,
    pub title: Option<String>,
    pub content: Option<String>,
    pub link: Option<String>,
    pub link_title: Option<String>

}