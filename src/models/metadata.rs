#[allow(dead_code)]

use chrono::prelude::*;
use rocket::serde::{Deserialize, Serialize};

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Metadata {
    created_datetime_iso: Option<String>,
    modified_datetime_iso: Option<String>,
    created_datetime_epoch: Option<i64>,
    modified_datetime_epoch: Option<i64>
}

impl Metadata {
    pub fn new() -> Metadata {
        Metadata {
            created_datetime_iso: Some(Utc::now().to_rfc3339()),
            modified_datetime_iso: Some(Utc::now().to_rfc3339()),
            created_datetime_epoch: Some(Utc::now().timestamp()),
            modified_datetime_epoch: Some(Utc::now().timestamp())
        }
    }

    pub fn update_modified(&mut self) -> Metadata {
        self.modified_datetime_iso = Some(Utc::now().to_rfc3339());
        self.modified_datetime_epoch = Some(Utc::now().timestamp());
        self.to_owned()
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct ModifiedMetadata {
    modified_datetime_iso: Option<String>,
    modified_datetime_epoch: Option<i64>
}

impl ModifiedMetadata {
    pub fn new() -> Self {
        ModifiedMetadata {
            modified_datetime_iso: Some(Utc::now().to_string()),
            modified_datetime_epoch: Some(Utc::now().timestamp())
        }
    }
}
