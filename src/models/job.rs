#[allow(dead_code)]

use rocket::serde::{Serialize, Deserialize};

use crate::models::metadata::Metadata;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Job {
    id: Option<String>,
    company_id: Option<String>,
    title: Option<String>,
    description: Option<String>,
    skill_list: Option<Vec<String>>,
    compensation: Option<String>,
    metadata: Option<Metadata>
}

impl Job {
    pub fn new(company_id: &str) -> Self {
        Job {
            id: None,
            company_id: Some(company_id.to_string()),
            title: None,
            description: None,
            skill_list: None, 
            compensation: None,
            metadata: Some(Metadata::new()) 
        }
    }
    
    pub fn get_id(&self) -> String {
        self.id.clone().unwrap_or_default()
    }

    pub fn get_company_id(&self) -> String {
        self.company_id.clone().unwrap_or_default()
    }

    pub fn get_title(&self) -> String {
        self.title.clone().unwrap_or_default()
    }

    pub fn get_description(&self) -> String {
        self.description.clone().unwrap_or_default()   
    }

    pub fn get_skill_list(&self) -> Vec<String> {
        self.skill_list.clone().unwrap_or_default()   
    }

    pub fn get_compensation(&self) -> String {
        self.compensation.clone().unwrap_or_default()   
    }

    pub fn get_metadata(&self) -> Metadata {
        self.metadata.clone().unwrap_or_default()
    }

    pub fn set_company_id(&mut self, value: &str) {
        self.company_id = Some(value.to_string());
    }

    pub fn set_title(&mut self, value: &str) {
        self.title = Some(value.to_string());
    }

    pub fn set_description(&mut self, value: &str) {
        self.description = Some(value.to_string());
    }

    pub fn set_skill_list(&mut self, value: Vec<String>) {
        self.skill_list = Some(value)
    }

    pub fn set_compensation(&mut self, value: &str) {
        self.compensation = Some(value.to_string());
    }

    pub fn set_metadata(&mut self, value: Metadata) {
        self.metadata = Some(value);
    }

}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewJob {
    pub id: Option<String>,
    pub company_id: String,
    pub title: String,
    pub description: String,
    pub skill_list: Vec<String>,
    pub compensation: String,
    pub metadata: Option<Metadata>
}