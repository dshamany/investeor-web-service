use rocket::{
    http::{Cookie, CookieJar},
    response::Redirect,
    serde::json::{Json, Value, json},
};
use serde_json;
use uuid::Uuid;

use crate::models::{surrealdb::Query, code_exchange::CodeExchange, session::Session, user::UserCredentials};
use crate::services::{surrealdb::{query2, query3, set_attributes}};

use reqwest::Client;

use super::surrealdb_functions::get_item_by_id;

#[get("/redirect")]
pub async fn redirection(jar: &CookieJar<'_>) -> Redirect {
    let code = Uuid::new_v4().to_string().replace("-", "")[0..=12].to_string();
    let cookie = Cookie::build("code", code)
        .secure(true)
        .http_only(false)
        .finish();
    jar.add(cookie);
    Redirect::to(uri!("http://localhost:8000/"))
}

#[post("/q", format = "json", data = "<query>")]
pub async fn sql(query: Json<Query>) -> Value {
    let endpoint = "http://localhost:4000/sql";
    let client = Client::new();
    let resp = query3(&query.to_string()).await;
    resp.clone()
}

#[post("/get_code", format = "json", data = "<user_credentials>")]
pub async fn get_code(user_credentials: Json<UserCredentials>) -> Json<Value> {
    println!("{:?}", user_credentials);
    let user_credentials = user_credentials.0.clone();
    let q = format!("SELECT * FROM user WHERE email=\"{}\" AND password=\"{}\"", user_credentials.email.unwrap_or_default(), user_credentials.password.unwrap_or_default());
    let resp = query2(&q).await;
    let result = &resp[0].result.clone();
    match result {
        Some(r) => {
            if r.is_empty() {
                return Json(json!({"error": "User not found"}))
            }
            let user_id = r[0]["id"].to_string().replace("\"", "");
            let session = Session::new(String::from(&user_id));
            let session_str = serde_json::to_string(&session).unwrap_or_default();
            let attributes = set_attributes(&session_str);
            let query = format!("DELETE session WHERE user_id={}; CREATE session SET {};", user_id, attributes);
        
            // We're not returning the values just yet
            // No need to capture this value
            query2(&query).await;
        
            return Json(json!({
            "code": session.get_code()
            }));
        },
        None => Json(json!({"error": "Invalid User"}))
    }


}

#[post("/exchange_code", format = "json", data = "<verification>")]
pub async fn exchange_code(verification: Json<CodeExchange>) -> Json<Value> {
    let query = format!(
        "SELECT * FROM session WHERE code=\"{}\"",
        verification.code.clone().unwrap_or_default()
    );
    let resp = query2(&query).await;
    let resp = resp[0].result.clone();

    match resp {
        Some(r) => {
            if r.len() < 1 {
                return Json(json!({"error": "Invalid Code"}));
            }

            let access_token = r[0]["id"].to_string();
            let access_token = access_token
                .to_string()
                .replace("session:", "")
                .replace("\"", "");
        
            // Remove the code to prevent further use
            let query = format!(
                "UPDATE session:{} SET code = null;",
                access_token
            );
            query2(&query).await;
        
            return Json(json!({"sessionId": access_token}));
        },
        None => Json(json!({"error": "Invalid Code"}))
    }

}


