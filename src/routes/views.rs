extern crate rocket_dyn_templates;

use rocket::http::{Cookie, CookieJar};
use rocket::serde::json::{Json, Value, from_value, json};
use rocket_dyn_templates::{Template, context};

use uuid::Uuid;

// use rocket_dyn_templates::handlebars::Handlebars;
// use serde_json::json;
// use std::{error::Error, string};

use crate::models::{user::User, card_view::CardView};

use super::surrealdb_functions::get_item_by_id;

#[get("/verify")]
pub async fn verify(jar: &CookieJar<'_>) -> Template {
    let code = Uuid::new_v4().to_string().replace("-", "")[0..=12].to_string();
    let cookie = Cookie::build("code", code.clone())
        .secure(false)
        .http_only(false)
        .finish();
    jar.add(cookie);
    Template::render("verify", context! {title: "Home", code: code.clone() })
}

#[get("/")]
pub async fn index() -> Template {
    let card_list = vec![
        CardView {
            id: None,
            title: Some("Carlos Miguel".to_string()),
            image: Some("https://cdn.fstoppers.com/styles/full/s3/media/2017/09/10/1_use_psychology_to_take_better_photographs.jpeg".to_string()),
            content: Some("I started a company that recycles poop.".to_string()),
            link: Some("#".to_string()),
            link_title: Some("see more".to_string())
        },
        CardView {
            id: None,
            title: Some("Lucy Bradfort".to_string()),
            image: Some("https://images.hivisasa.com/1200/It9Rrm02rE20.jpg".to_string()),
            content: Some("I started a company that wastes people's time.".to_string()),
            link: Some("#".to_string()),
            link_title: Some("see more".to_string())
        },
        CardView {
            id: None,
            title: Some("Jonathan Sick".to_string()),
            image: Some("https://www.calmsage.com/wp-content/uploads/2020/07/Face-The-People.png".to_string()),
            content: Some("I started a company makes people sick.".to_string()),
            link: Some("#".to_string()),
            link_title: Some("see more".to_string())
        }
    ];
    Template::render("home", context! { title: "Home",
                                        cards: card_list 
                                    })
}

#[get("/login")]
pub async fn login_form() -> Template {
    Template::render("login_form", context!{})
}

#[get("/register")]
pub async fn register_form() -> Template {
    Template::render("register_form", context!{})
}

#[get("/code_exchange")]
pub async fn code_exchange() -> Template {
    Template::render("code_exchange_form", context! {title: "Code Exchange"})
}

#[get("/view/<template>")]
pub async fn get_template(template: String) -> Template {
    Template::render(template.clone(), context! { title: template.clone() })
}

#[get("/contact")]
pub async fn get_contact() -> Template {
    Template::render("contact", context! {title: "Contact"})
}

#[get("/about")]
pub async fn get_about() -> Template {
    Template::render("about", context! {title: "About"})
}

#[get("/investors")]
pub async fn get_investors() -> Template {
    Template::render("investors", context! {title: "Investors"})
}

#[get("/startups")]
pub async fn get_startups() -> Template {
    Template::render("startups", context! {title: "Startups"})
}

#[get("/talent")]
pub async fn get_talent() -> Template {
    Template::render("talent", context! {title: "Talent"})
}

#[get("/profile/<id>")]
pub async fn get_profile(id: String) -> Template {

    let item = get_item_by_id("user".to_string(), id).await;
    let item = &item.0[0]["result"][0];
    
    let item_json = json!({
        "email": &item["email"],
        "firstname": &item["firstname"],
        "lastname": &item["lastname"]
    });

    // let user: User = serde_json::from_value(item.clone()).unwrap();
    println!("{}", &item_json.to_string());

    Template::render("profile", context! { 
        title: "Profile", 
        user: item_json.to_string()
    })
}
