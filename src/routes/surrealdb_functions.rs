use std::env;
use dotenv::dotenv;
use rocket::serde::json::{Json, Value, json};
use crate::services::surrealdb::{set_attributes, query, create_query_metadata, update_query_metadata};

// CREATE

#[post("/user", format = "json", data = "<data>")]
pub async fn create_user(data: Json<Value>) -> Json<Value> {
    dotenv().ok();

    if (data.0["code"].is_null() || 
        data.0["fullname"].is_null() || 
        data.0["email"].is_null() ||
        data.0["password"].is_null()){
        return Json(json!({"error": "Missing Fields"}));
    }

    // Signup Code validation
    let signup_code = env::var("SIGNUP_CODE").unwrap_or(String::new());
    if data.0["code"].is_null() || data.0["code"] != signup_code.to_string() {
        return Json(json!({"error": "Invalid signup code"}));
    }

    let is_valid_user = !user_exists(data.0["email"].to_string()).await;

    if is_valid_user {
        return Json(json!({"error": "User already exists"}));
    }


    let data = serde_json::to_string(&data.0).unwrap_or_default();
    let data = set_attributes(&data);
    let q = format!("CREATE user SET {};", &data);
    let resp = query(&q).await;

    // create metadata
    let resp2: Value = serde_json::from_str(&resp).unwrap_or_default();
    let id = resp2[0]["result"][0]["id"].to_string();
    let q = create_query_metadata(&id);
    let resp3 = query(&q).await;

    let mut value: Value = serde_json::from_str(&resp3).unwrap_or_default();
    value[0]["result"][0]["password"].take(); 
    Json(value)
}

#[post("/<table_name>", format = "json", data = "<data>")]
pub async fn create_item(table_name: String, data: Json<Value>) -> Json<Value> {
    let data = serde_json::to_string(&data.0).unwrap_or_default();
    let data = set_attributes(&data);
    let q = format!("CREATE {} SET {};", &table_name, &data);
    let resp = query(&q).await;

    // create metadata
    let resp2: Value = serde_json::from_str(&resp).unwrap_or_default();
    let id = resp2[0]["result"][0]["id"].to_string();
    let q = create_query_metadata(&id);
    let resp3 = query(&q).await;

    let value: Value = serde_json::from_str(&resp3).unwrap_or_default();
    Json(value)
}

// READ
#[get("/<table_name>")]
pub async fn get_all_items(table_name: String) -> Json<Value> {
    let q = format!("SELECT * OMIT password, code FROM {};", &table_name);
    let resp = query(&q).await;
    let value: Value = serde_json::from_str(&resp).unwrap_or_default();
    Json(value)
}

#[get("/user/<id>")]
pub async fn get_user_by_id(id: String) -> Json<Value> {
    let q = format!("SELECT * OMIT password, code FROM user WHERE id=user:{}", &id);
    let resp = query(&q).await;
    let value: Value = serde_json::from_str(&resp).unwrap_or_default();
    Json(value)
}

#[get("/<table_name>/<attribute>/<value>")]
pub async fn get_item_by_user_id(table_name: String, attribute: String, value: String) -> Json<Value> {
    let q = format!("SELECT * OMIT password, code FROM {} WHERE {}='{}'", &table_name, &attribute, &value);
    let resp = query(&q).await;
    let mut value: Value = serde_json::from_str(&resp).unwrap_or_default();

    Json(value)
}

#[get("/<table_name>/<id>")]
pub async fn get_item_by_id(table_name: String, id: String) -> Json<Value> {
    let q = format!("SELECT * OMIT password, code FROM {} WHERE id={}:{}", &table_name, &table_name, &id);
    let resp = query(&q).await;
    let mut value: Value = serde_json::from_str(&resp).unwrap_or_default();

    Json(value)
}

// UPDATE
#[put("/<table_name>/<id>", format = "json", data = "<data>")]
pub async fn update_item_by_id(table_name: String, id: String, data: Json<Value>) -> Json<Value> {
    let data = serde_json::to_string(&data.0).unwrap_or_default();
    let data = set_attributes(&data);
    let q = format!("UPDATE {}:{} SET {}", &table_name, &id, &data);
    let resp = query(&q).await;

    // update metadata
    let resp2: Value = serde_json::from_str(&resp).unwrap_or_default();
    let id = resp2[0]["result"][0]["id"].to_string();
    let q = update_query_metadata(&id);
    let resp3 = query(&q).await;

    let value: Value = serde_json::from_str(&resp3).unwrap_or_default();
    Json(value)
}

// DELETE
#[delete("/<table_name>/<id>")]
pub async fn delete_item_by_id(table_name: String, id: String) -> Json<Value> {
    let q = format!("DELETE {}:{};", &table_name, &id);
    let resp = query(&q).await;
    let resp = serde_json::from_str(&resp).unwrap_or_default();
    Json(resp)
}

pub async fn user_exists(email: String) -> bool {
    let q = format!("SELECT * FROM user WHERE email={};", email);
    let resp = query(&q).await;
    let resp: Value = serde_json::from_str(&resp).unwrap_or_default();
    let resp = &resp[0]["result"][0];
    resp.is_null()
}


